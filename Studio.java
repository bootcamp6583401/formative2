class Studio {
    private String name;
    private int seatsAmount;

    public Studio(String name, int seatsAmount) {
        this.setName(name);
        this.setSeatsAmount(seatsAmount);
    }

    @Override
    public String toString() {
        return String.format("Name: %s, Seats: %d", this.getName(), this.getSeatsAmout());
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeatsAmout() {
        return this.seatsAmount;
    }

    public void setSeatsAmount(int seatsAmount) {
        this.seatsAmount = seatsAmount;
    }
}