import java.util.ArrayList;

class Bioskop {
    private String name;
    private String address;
    private String openingHours;
    private ArrayList<Employee> employees;
    private ArrayList<Studio> studios;
    private ArrayList<Movie> movies;

    public Bioskop(String name, String address, String openingHours) {
        this.setName(name);
        this.setAddress(address);
        this.setOpeningHours(openingHours);
        this.employees = new ArrayList<Employee>();
        this.movies = new ArrayList<Movie>();
        this.studios = new ArrayList<Studio>();
    }

    @Override
    public String toString() {

        String printRes = "BIOSKOP\n============\nNama: " + this.getName() + "\nAddress: " + this.getAddress();

        printRes += "\n\nEMLOYEES\n===========";
        for (Employee employee : this.employees) {
            printRes += String.format("\n-%s\n", employee.toString());
        }

        printRes += "\n\nSTUDIOS\n===========";
        for (Studio studio : this.studios) {
            printRes += String.format("\n-%s", studio.toString());
        }

        printRes += "\n\nMOVIES\n===========";
        for (Movie movie : this.movies) {
            printRes += String.format("\n-%s", movie.toString());
        }

        return printRes;
    }

    public void addStudio(Studio studio) {
        this.studios.add(studio);
    }

    public void addMovie(Movie movie) {
        this.movies.add(movie);
    }

    public void hire(Employee employee) {
        this.employees.add(employee);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOpeningHours() {
        return this.openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }
}