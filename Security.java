/**
 * Security
 */
public class Security extends Employee {
    private String[] certification;
    private String[] securityGears;

    public Security(String name, String startDate, int salary, int startWorkingHour, int endWorkingHour,
            String uniform, String[] certification, String[] securityGears) {
        super(name, startDate, salary, startWorkingHour, endWorkingHour, uniform);
        this.setCertification(certification);
        this.setSecurityGears(securityGears);
    }

    @Override
    public String toString() {
        String printRes = "SECURITY " + super.toString() + "\nCertifications: ";
        for (String cert : certification) {
            printRes += String.format("\n-%s", cert);
        }
        printRes += "\nSecurity Gears: ";
        for (String gear : securityGears) {
            printRes += String.format("\n-%s", gear);
        }
        return printRes;
    }

    public String[] getCertification() {
        return certification;
    }

    public void setCertification(String[] certification) {
        this.certification = certification;
    }

    public String[] getSecurityGears() {
        return securityGears;
    }

    public void setSecurityGears(String[] securityGears) {
        this.securityGears = securityGears;
    }

}