/**
 * Cashier
 */
public class Cashier extends Employee {
    private int hasilPenjualanTicket;

    public Cashier(String name, String startDate, int salary, int startWorkingHour, int endWorkingHour,
            String uniform, int hasilPenjualanTicket) {
        super(name, startDate, salary, startWorkingHour, endWorkingHour, uniform);
        this.setHasilPenjualanTicket(hasilPenjualanTicket);
    }

    @Override
    public String toString() {
        return "CASHIER " + super.toString() + "\nHasil Penjualan Ticket: " + this.getHasilPenjualanTicket();
    }

    public int getHasilPenjualanTicket() {
        return hasilPenjualanTicket;
    }

    public void setHasilPenjualanTicket(int hasilPenjualanTicket) {
        this.hasilPenjualanTicket = hasilPenjualanTicket;
    }

}