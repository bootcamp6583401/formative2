abstract class Employee {
    private String name;
    private String startDate;
    private int salary;
    private int startWorkingHour;
    private int endWorkingHour;
    private String uniform;

    public Employee(String name, String startDate, int salary, int startWorkingHour, int endWorkingHour,
            String uniform) {
        this.name = name;
        this.startDate = startDate;
        this.salary = salary;
        this.startWorkingHour = startWorkingHour;
        this.endWorkingHour = endWorkingHour;
        this.uniform = uniform;
    }

    @Override
    public String toString() {
        return "EMPLOYEE\n*********\nNama: " + this.getName() + "\nStart Date: " + this.getStartDate()
                + "\nSalary: " + this.getSalary() + "\nStart Hour: " + this.getStartWorkingHour() + "\nEnd Hour: "
                + this.getEndWorkingHour() + "\nUniform: " + this.getUniform();
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getStartWorkingHour() {
        return startWorkingHour;
    }

    public void setStartWorkingHour(int startWorkingHour) {
        this.startWorkingHour = startWorkingHour;
    }

    public int getEndWorkingHour() {
        return endWorkingHour;
    }

    public void setEndWorkingHour(int endWorkingHour) {
        this.endWorkingHour = endWorkingHour;
    }

    public String getUniform() {
        return uniform;
    }

    public void setUniform(String uniform) {
        this.uniform = uniform;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}