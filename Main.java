public class Main {
    public static void main(String[] args) {
        Bioskop bioskop = new Bioskop("XXI", "Disini", "8 - 10");

        bioskop.hire(new Cashier("Jim Halpert", "2023-17-01", 3000000, 8, 16, "Suit", 3));
        bioskop.hire(new Cashier("Pam Beesley", "2023-17-01", 3000000, 8, 16, "Suit", 3));

        bioskop.hire(new Security("Kevin Malone", "2022-11-22", 3500000, 6, 8, "Security Uniform",
                new String[] { "Sertifikat Super", "Sertifikat Sekuritas" }, new String[] { "Ketukan", "Pluit" }));

        bioskop.hire(new Cleaning("Dwight Schrute", "2021-01-01", 5000000, 4, 18, "Suit",
                new String[] { "Kain Pel", "Sapu" }));
        bioskop.hire(new Cleaning("Oscar Martinez", "2024-01-01", 2500000, 4, 18, "Bebas",
                new String[] { "Kain Pel", "Sapu", "Daia" }));

        bioskop.addStudio(new Studio("Studio 1", 20));
        bioskop.addStudio(new Studio("Studio 2", 25));
        bioskop.addStudio(new Studio("Studio 3", 20));

        bioskop.addMovie(new Movie(30000, "Shawshank Redemption"));
        bioskop.addMovie(new Movie(25000, "Saving Private Ryan"));
        bioskop.addMovie(new Movie(45000, "Inglorious Basterds"));
        bioskop.addMovie(new Movie(30000, "Django Unchained"));
        bioskop.addMovie(new Movie(25000, "Goodfellas"));

        System.out.println(bioskop);
    }
}
