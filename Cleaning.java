/**
 * Cleaning
 */
public class Cleaning extends Employee {
    private String[] cleaningGears;

    public Cleaning(String name, String startDate, int salary, int startWorkingHour, int endWorkingHour,
            String uniform, String[] cleaningGears) {
        super(name, startDate, salary, startWorkingHour, endWorkingHour, uniform);
        this.setCleaningGears(cleaningGears);
    }

    @Override
    public String toString() {
        String printRes = "SECURITY " + super.toString() + "\nCleaning Gears: ";
        for (String gear : cleaningGears) {
            printRes += String.format("\n-%s", gear);
        }
        return printRes;
    }

    public String[] getCleaningGears() {
        return cleaningGears;
    }

    public void setCleaningGears(String[] cleaningGears) {
        this.cleaningGears = cleaningGears;
    }

}