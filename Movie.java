/**
 * Movie
 */
public class Movie {

    private int price;
    private String title;

    public Movie(int price, String title) {
        this.setPrice(price);
        this.setTitle(title);
    }

    @Override
    public String toString() {
        return String.format("Title: %s, Price: Rp%d", this.getTitle(), this.getPrice());
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}